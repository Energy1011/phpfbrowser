<?php
/*
*Sync php file, this is used to create files and folders list then insert then to db
* you need to call this script each time you put a new file in rootdir of files
*Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

// Run command in terminal $find files/ -follow -type f > file.txt
include('src/bootstrap.php');

/**
 * Return a new insert string 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 * @access  public
 * @return  String 
 */ 
function reset_insert_string(){
	return 'INSERT INTO file (parent_path, path, filename) VALUES '; 
}

/**
 * Do a query to insert 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 * @access  public
 * @uses    db->query()
 * @param   String $insert inert into string
 * @return  Nothing 
 */ 
function insert_to_db($insert){
	$db = new DB;
	$result = $db->query($insert.";"); 
	//echo $insert;
}

/**
 * Return parent string from a complete path 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 * @access  public
 * @param   String $path cpmplete path route 
 * @return  String 
 */ 
function get_parent_path($path){
	return substr($path, 0, strripos($path, '/'));
}

/**
 * Get file list of files from file.txt list 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 * @access  Public
 */ 
function get_files(){
	$insert = reset_insert_string(); 
	$file = fopen("file.txt", "r");
	$n_line = 1;
	while(!feof($file)){
		$line = trim(fgets($file));
		if(!empty($line)){
				if($n_line > 1) $insert.=',';
				$path = dirname($line);	
				$insert.= "('".$path."','".$path."','".basename($line)."')";
				$line = '';
		}
	 	$n_line++;
	// it prevents a huge $insert string, dividing inserts to db each 5000 elements
		if($n_line  > 100){
			insert_to_db($insert);
			$insert = reset_insert_string();
			$n_line = 1; 
		}
 		
	}
	insert_to_db($insert);
	fclose($file);
}

/**
 * Get dirs from file dirs.txt to insert to db 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function get_paths(){
	$insert = reset_insert_string(); 
	$file = fopen("dir.txt", "r");
	$n_line = 1;
	$last = '';
	while(!feof($file)){
		$line = trim(fgets($file));
		if(!empty($line)){
				if($n_line > 1) $insert.=',';
				$parent_path = trim(get_parent_path($line));	
				$insert.= "('".$parent_path."','".$line."','NULL')";
				$line = '';
		}
		$n_line++;
	// it prevents a huge $insert string, dividing inserts to db each 5000 elements
		if($n_line  > 100){
				insert_to_db($insert);
				$insert = reset_insert_string();
				$n_line = 1; 
		}
		
	}
	insert_to_db($insert);
	fclose($file);
}

/**
 * Read files of files and dirs
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function read_file(){
	$db = new DB;
	$db->query("TRUNCATE TABLE file;"); 
	echo "\n[i] Inserting data structure to db";
	get_paths();
	get_files();
}

/**
 * Main function 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function main(){
	echo "[i] Running php script";
	read_file();
	echo "\n[!] Php script finish\n";
}

main();
?>
