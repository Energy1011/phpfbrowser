<?php 
	// Includes
	include('src/bootstrap.php');
	//Session
	session_start();
	$_SESSION["pwd"] = $app->config['rootdir'];

?>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php lang('file_browser'); ?></title>
	<!-- Bootstrap css--!>
	<link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css"> 
	<script src="js/jquery/jquery.min.js"></script>
	<script src="js/bootstrap/js/bootstrap.min.js"></script>
	<script src="js/app.js"></script>
<?php 
	echo "<script>";
	// if get cd is set, then change dir to the path
	if (isset($_GET['gp'])){
		echo "
			cd('".$_GET['gp']."',0);
		";
	}else{
		echo "
			// On document ready
			$(document).ready(function(){
			// Get dir
			cd('', 0);
		});";
	}
	echo "</script>";
?>
<style>
body {
	margin-left: 5px;
}
</style>
</head>
<body>
	<div>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
			<div class="navbar-header">
				<span class="navbar-text navbar-left"><?php lang('file_browser'); ?><a href="http://www.conafor.gob.mx" class="navbar-link"><?php echo " ".$app->config['brand_title']; ?></a></span>
			</div>
		  </div>
		</nav>
	</div>
	<div>
		<h3><span class="label label-default"><?php lang('index_of'); ?></span> <span id="div-curr-directory"></span></h3> 
	</div>
	<p id="url-to-copy"><?php lang('dir_not_exist'); ?></p>
	<div class="row">
		<div class="navbar-form navbar-left" role="search">
			<div class="form-group">
				<input id="entry-search" placeholder='<?php lang('search'); ?>'></input>
			</div>
				<button  class="btn btn-default" onclick="search()"><?php lang('search');?></button>
		</div>
	</div>
	<div class="panel panel-default">
		<div>
			<span><?php lang('list_of_contents'); ?></span>
		</div>
	<div class="panel-heading">
	<div>
		<nav aria-label="Page navigation">
		<div>
			<div>
				<ul id="div-control-p" class="pagination" style="margin: -9px 5px;"></ul>	
			</div>
		</nav>
		</div>
	</div>
</div>
	  <div class="panel-body">
		<div id="btn-parent-directory"></div>
		<div id='div-list'></div>
	  </div>
	  <div class="panel-footer"></div>
	</div>
<script></script>
</body>
</html>
