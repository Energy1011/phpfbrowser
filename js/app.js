// appData global JSON general data
var appData= { 
	"curr_page_num": 0,
	"parent_dir": "",
	"curr_dir": "",
	"total_items":0,
	"page_size":0
	};

/**
 * Set and show the control with number of pages to navigate files 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function show_control_pages(){
	// Reload content
	$('#div-control-p').html('');
	var total_pages = appData.total_items / appData.page_size;
	for(var e=0; e<= total_pages; e++){
		// Append number button to navigate btwn pages
		$('#div-control-p').append('<li><a onclick="cd(\''+appData.curr_dir+'\','+e+');">'+parseInt(e+1)+'</a></li>');
	}
}

/**
 * Search file function
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function search(){
	//var search = $("#entry-search").val();
	var search = $("#entry-search").val();
	$.ajax({
		url: "controller.php",
		dataType: "json",
		method: "GET",
		data: {  func:"search", path:'', curr_page_num: 0, term:search}
	}).done(function(response) {
		if(response.code == 200){
			show_files(response);
		}
	});
}

/**
 * prompt current url to copy  
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 * @param   string url
 */ 
function show_curr_url(url) {
    window.prompt("Url, Control+C to copy", url);
}

/**
 * Show all the conent of the current dir, iter over all files 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function show_files(data){
	appData.curr_page_num = data.curr_page_num;
	appData.parent_dir = data.parent_dir;
	appData.curr_dir = data.curr_dir;
	appData.total_items = data.total_items;
	appData.page_size = data.page_size;
	var url = window.location.href+'?gp='+appData.curr_dir;
	url = url.match(/(http.*\/)\?/); url = url[1]+"?gp="+appData.curr_dir;
	$("#url-to-copy").html('<button onclick="show_curr_url(\''+url+'\')">Mostrar url</button>');
	// set data
	data = data.data;
	// Clear div list
	$('#div-list').html('');
	for(var item in data){
		var fullpath = data[item].path;
		if(data[item].filename != 'NULL'){
			// is a file
			var filename = data[item].filename.trim();
			var element = '<p><a href="'+appData.curr_dir+"/"+filename+'"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>'+filename+'</a></p>';
			$('#div-list').append(element);
		}else{
			// is a dir
			var path = data[item].path.trim();
			var element = '<p><a onclick="cd(\''+path+'\');"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> '+path+'</a></p>';
			$('#div-list').append(element);
		}
	}
	// Set parent dir 
	$('#btn-parent-directory').html('');
	$('#btn-parent-directory').append('<p onclick="cd(\''+appData.parent_dir+'\');"><span class="glyphicon glyphicon-arrow-up"></span> Ir Directorio Padre</p>');
	$('#div-curr-directory').html('<span class="glyphicon glyphicon-folder-open"></span> '+appData.curr_dir);
	show_control_pages();
}

/**
 * Change dir 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function cd(dir, page_num){
	if( typeof page_num == 'undefined') page_num = 0;
	// Ajax to get dir
	$.ajax({
		url: "controller.php",
		dataType: "json",
		method: "GET",
		data: {  func:"cd", path:dir, curr_page_num: page_num}
	}).done(function(response) {
		if(response.code == 200){
			show_files(response);
		}
	});
}
