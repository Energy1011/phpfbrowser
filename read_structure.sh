#/bin/bash 

#This script needs to be called each time you make changes in the rootdir (add, rename, delete) files
# you can call this script with a cron job
# you can call this script with a cron job
#Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#Get rootdir value
rootdir=`grep config.json -e '"rootdir":\(.*\)' | sed 's/\s"rootdir":"//g' | sed 's/",//g'`

echo "[i] Getting data structure...";
find $rootdir/ -follow -type d > dir.txt 
find $rootdir/ -follow -type f > file.txt
#Run php sync script
php sync.php
rm -f dir.txt
rm -f file.txt 
