<?php 
/**
 * DB connection class 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
class DB{

	/**
	 * Open a connection to db 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
	 * @return  Object mysql connection 
	 */ 
	function open_connection(){
		global $app;
		$con =
			mysql_connect($app->config['dbserver'],$app->config['dbusername'],$app->config['dbpassword']);
			mysql_select_db($app->config['dbname']);
		if(!$con){
			die('Could not connect:'.mysql_error());
		}else{
			return $con;
		}
	}

	/**
	 * Close a connection 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
	 * @param   Object mysql connection to close 
	 * @return  Boolean 
	 */ 
	function close_connection($con){	
		return mysql_close($con);
	}

	/**
	 * Execute a query
	 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
	 * @param   String $query
	 * @return  Mysql result 
	 */ 
	function query($query){
		global $app;
		$con = $this->open_connection();
		$result = mysql_query($query);
		if (!$result) {
			    die('Invalid query: '.mysql_error());
		}
		$this->close_connection($con);
		return $result;
	}

}
?>
