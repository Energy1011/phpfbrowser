<?php

//load config file
$app = New Phpfbrowser;
$app->load_config();

//Load lang functions
include('src/lang.php');
include('src/db.php');

/**
 * Main class to bootstrap the app object
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
class Phpfbrowser{
	public $config = Array();

	/**
	 * Load config file in json format into the main object app->config 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
	 */ 
	function load_config(){
		$file = file_get_contents('config.json');
		// parse json to global array
		$this->config = json_decode($file, true);
	}
}

?>
