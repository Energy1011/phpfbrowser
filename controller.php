<?php 
include('src/bootstrap.php');

/**
 * Search file in database 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 * @access  Access
 * @uses    Required functions
 * @param   $_GET path, curr_page_num, term, etc 
 * @return  JSON 
 */ 
function search_get(){
	global $app;
	// Check for actual path, if is not set then redirect to rootdir
	if(!isset($_GET['path']) ||  $_GET['path'] == '')
		$_GET['path'] = $app->config['rootdir'];
	//TODO: check for validation always root start with rootdir
	if(!isset($_GET['curr_page_num']) ||  $_GET['curr_page_num'] == '')
		$_GET['curr_page_num'] = 0;

	$curr_page_num = mysql_escape_string($_GET['curr_page_num']);
	if($curr_page_num <= 0){
		$curr_page_num = 0;
	}

	$path = mysql_escape_string($_GET['path']);
	$term= mysql_escape_string($_GET['term']);

	$db = new DB;
	$start = $curr_page_num * $app->config['page_size'];
	$end = $start + $app->config['page_size'];

	// First I get the total rows
	$query = "SELECT path FROM file WHERE parent_path = '{$path}'";
	$total= $db->query($query);
	$total_items = mysql_num_rows($total);

	// Now I get rows with limit
	$query = "SELECT path, filename FROM file WHERE path or filename like '%{$term}%'";
	$result = $db->query($query);
	$rows = array();

	while($r = mysql_fetch_assoc($result)) {
		    $rows[] = $r;
	}

	if(mysql_num_rows($result) >= 1){
		$code = 200;
	}else{
		$code = 404;
	}

	$parent_path = substr($path, 0, strripos($path, '/'));
	// Response
	echo json_encode(array(
		'code' => $code,
		'data' => $rows,
		'parent_dir' => $parent_path,
		'curr_dir' => $path,
		'curr_page_num' => $curr_page_num,
		'page_size' => $app->config['page_size'],
		'total_items' => $total_items));
}

/**
 * Change dir 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 * @access  Access
 * @uses    Required functions
 * @param   $_GET path, curr_page_num etc 
 * @return  JSON 
 */ 
function cd_get(){
	global $app;
	// Check for actual path, if is not set then redirect to rootdir
	if(!isset($_GET['path']) ||  $_GET['path'] == '')
		$_GET['path'] = $app->config['rootdir'];
	//TODO: check for validation always root start with rootdir
	if(!isset($_GET['curr_page_num']) ||  $_GET['curr_page_num'] == '')
		$_GET['curr_page_num'] = 0;

	$curr_page_num = mysql_escape_string($_GET['curr_page_num']);
	if($curr_page_num <= 0){
		$curr_page_num = 0;
	}

	$path = mysql_escape_string($_GET['path']);

	$db = new DB;
	$start = $curr_page_num * $app->config['page_size'];
	$end = $start + $app->config['page_size'];

	// First I get the total rows
	$query = "SELECT path FROM file WHERE parent_path = '{$path}'";
	$total= $db->query($query); 
	$total_items = mysql_num_rows($total);

	// Now I get rows with limit
	$query = "SELECT path, filename FROM file WHERE parent_path = '{$path}' LIMIT {$start}, {$app->config['page_size']}";
	$result = $db->query($query); 
	$rows = array();

	while($r = mysql_fetch_assoc($result)) {
		    $rows[] = $r;
	}

	if(mysql_num_rows($result) >= 1){
		$code = 200;
	}else{
		$code = 400;
	}

	$parent_path = substr($path, 0, strripos($path, '/'));
	// Response 
	echo json_encode(array(
		'code' => $code,
		'data' => $rows,
		'parent_dir' => $parent_path,
		'curr_dir' => $path,
		'curr_page_num' => $curr_page_num,
		'page_size' => $app->config['page_size'],
		'total_items' => $total_items));
}

/**
 * Dispatch request http and call the function
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function dispatch(){
	//TODO: clear input
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$call = $_POST['func']."_post";	
	}

	if ($_SERVER['REQUEST_METHOD'] === 'GET') {
		$call = $_GET['func']."_get";	
	}

	if(function_exists($call)){
		$call();
	}
}

// Dispatch request
dispatch();
?>
