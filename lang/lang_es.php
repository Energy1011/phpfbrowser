<?php
// Spanish lang 
$LANG['file_browser'] = "Navegador de archivos";
$LANG['parent_directory'] = "Directorio padre";
$LANG['goto_parent_directory'] = "Ir Directorio Padre";
$LANG['current_path'] = "Directorio Actual";
$LANG['search'] = "Buscar";
$LANG['files'] = "Archivos";
$LANG['name'] = "Nombre";
$LANG['size'] = "Tamaño";
$LANG['index_of'] = "Índice de :";
$LANG['list_of_contents'] = "Listado de contenidos";
$LANG['dir_not_exist'] = "El directorio no existe";
?>
